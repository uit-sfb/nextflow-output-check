name := "nextflow-output-check"
scalaVersion := "2.12.8"
organization := "no.uit.sfb"
organizationName := "SfB"
version := "0.1.0-SNPASHOT"

ThisBuild / resolvers ++= {
  Seq(Some("Artifactory" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local/"),
    if ((version).value.endsWith("-SNAPSHOT"))
      Some("Artifactory-dev" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local/")
    else
      None
  ).flatten
}

lazy val scalaUtilsVersion = "0.2.1"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.5" % Test,
  "com.github.scopt" %% "scopt" % "4.0.0-RC2",
  "no.uit.sfb" %% "scala-utils-common" % scalaUtilsVersion,
  "no.uit.sfb" %% "scala-utils-json" % scalaUtilsVersion,
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.11.0",
  "com.fasterxml.jackson.dataformat" % "jackson-dataformat-csv" % "2.11.0"
)

enablePlugins(JavaAppPackaging, DockerPlugin)
//dockerLabels := Map("gitCommit" -> s"${git.formattedShaVersion.value.getOrElse("unknown")}@${git.gitCurrentBranch.value}")
dockerRepository in Docker := Some("registry.gitlab.com")
dockerUsername in Docker := Some("uit-sfb/nextflow-output-check")
//dockerChmodType := DockerChmodType.Custom("u=rX,g=rX,o=rX")
//daemonUser in Docker := "sfb" //"in Docker" needed for this parameter

enablePlugins(BuildInfoPlugin)
buildInfoKeys := Seq[BuildInfoKey](
  name,
  version,
  scalaVersion,
  sbtVersion
)