package no.uit.sfb

import java.nio.file.Paths

import no.uit.sfb.nextflow.TraceParser
import org.scalatest.{FunSpec, Matchers}
import no.uit.sfb.nextflow.TraceParser.Trace

class TraceParserTest extends FunSpec with Matchers {
  private val tracePath =
    Paths.get(getClass.getResource("/trace.tsv").getPath)

  describe(classOf[TraceParserTest].getName) {
    it("should parse trace") {
      val trace: Trace = TraceParser.fromTsv(tracePath)
      trace.head.hash should be("95/3aed55")
      trace.last.name should be("Assembly:Seqprep (2)")
    }
  }
}
