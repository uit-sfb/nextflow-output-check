package no.uit.sfb

import java.nio.file.Paths

import no.uit.sfb.nextflow._
import org.scalatest.{FunSpec, Matchers}

class DslTest extends FunSpec with Matchers {
  private val cfg =
    Config(Paths.get(getClass.getResource("/").getPath),
           "trace.tsv",
           Paths.get(getClass.getResource("/rules.tsv").getPath))

  //Note we cannot test stderr/out because the hidden files are ignored by sbt when building the resources

  describe(classOf[DslTest].getName) {
    val dsl = new DSL(cfg)
    it("should check all rules successfully") {
      dsl.check()
    }
  }
}
