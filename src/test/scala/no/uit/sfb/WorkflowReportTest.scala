package no.uit.sfb

import java.nio.file.Paths

import no.uit.sfb.nextflow.{
  AggregatedOutputFileSize,
  OutputFileExists,
  OutputFileSize,
  OutputFileSizeAbout,
  RuleBuilder,
  StdoutContains,
  WorkflowReportBuilder
}
import org.scalatest.{FunSpec, Matchers}

class WorkflowReportTest extends FunSpec with Matchers {
  private val cfg =
    Config(Paths.get(getClass.getResource("/").getPath), "trace.tsv")

  //Note we cannot test stderr/out because the hidden files are ignored by sbt when building the resources

  describe(classOf[WorkflowReportTest].getName) {
    val workflowReport = new WorkflowReportBuilder(cfg).build()
    val rb = RuleBuilder(workflowReport)
    it("should detect failed workflow") {
      workflowReport.workflowFailed should be(true)
    }
    it("should have the right number of outputs for the first task") {
      workflowReport.tasks.head.outputs.size should be(4)
    }
    it("should have the right outputs") {
      val tr = workflowReport.tasks.head
      OutputFileExists(tr, "unmerged_r1.fastq.gz").success should be(true)
      OutputFileSize(tr, "unmerged_r2.fastq.gz", 2506063).success should be(
        true)
      OutputFileSizeAbout(tr, "merged.fastq.gz", 450).success should be(true)
      StdoutContains(tr, "123").success should be(false)
    }
    it("should have right aggregated size for merged.fastq.gz") {
      AggregatedOutputFileSize(rb.tasksStartingWith("Assembly:Seqprep"),
                               "merged.fastq.gz",
                               3059).success should be(true)
    }
    it("should have right aggregated size for unmerged_r1.fastq.gz") {
      AggregatedOutputFileSize(rb.tasksStartingWith("Assembly:Seqprep"),
                               "unmerged_r1.fastq.gz",
                               10001922).success should be(true)
    }
    it("should have merged.fastq.gz everywhere") {
      rb.forall("Assembly:Seqprep", OutputFileExists(_, "merged.fastq.gz"))
    }
    it("should have merged.fastq.gz at least once") {
      rb.atLeastOne("Assembly:Seqprep", OutputFileExists(_, "merged.fastq.gz"))
    }
    it("should have test.txt at least once") {
      rb.atLeastOne("Assembly:Seqprep", OutputFileExists(_, "testDir/test.txt"))
    }
    it("should have right size for test.txt") {
      AggregatedOutputFileSize(rb.tasksStartingWith("Assembly:Seqprep"),
                               "testDir/test.txt",
                               4).success should be(true)
    }
  }
}
