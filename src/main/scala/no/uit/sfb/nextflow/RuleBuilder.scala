package no.uit.sfb.nextflow

case class RuleBuilder(workflowReport: WorkflowReport) {
  def tasksStartingWith(taskName: String): Seq[TaskReport] = {
    workflowReport.tasks.filter(_.name.startsWith(taskName))
  }

  def forall(taskName: String, check: TaskReport => RuleLike): Seq[Result] = {
    val tasks = tasksStartingWith(taskName)
    if (tasks.isEmpty)
      Seq(Failure(s"No task matched '$taskName'"))
    else
      tasksStartingWith(taskName) map { t =>
        check(t).checkRule()
      }
  }

  def atLeastOne(taskName: String,
                 check: TaskReport => RuleLike): Seq[Result] = {
    val (successes, failures) = forall(taskName, check) partition {
      _.isSuccess
    }
    if (successes.nonEmpty)
      successes
    else
      failures
  }

  def aggregate(taskName: String,
                check: Seq[TaskReport] => RuleLike): Seq[Result] = {
    val tasks = tasksStartingWith(taskName)
    if (tasks.isEmpty)
      Seq(Failure(s"No task matched '$taskName'"))
    else
      Seq(check(tasks).checkRule())
  }
}
