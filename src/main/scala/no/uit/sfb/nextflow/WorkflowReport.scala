package no.uit.sfb.nextflow

import no.uit.sfb.Config

case class WorkflowReport(tasks: Seq[TaskReport]) {
  val workflowFailed = tasks exists { _.failed }
}

class WorkflowReportBuilder(config: Config) {
  def build(): WorkflowReport = {
    val trace = TraceParser.fromTsv(config.workdir.resolve(config.traceName))
    val trb = new TaskReportBuilder(config)
    WorkflowReport(trace map {
      trb.from
    })
  }
}
