package no.uit.sfb.nextflow

import no.uit.sfb.Config
import no.uit.sfb.scalautils.common.FileUtils

class DSL(config: Config) {
  lazy val workflowReport = (new WorkflowReportBuilder(config)).build()

  def check(): Unit = {
    val rb = new RuleBuilder(workflowReport)
    val lines = FileUtils
      .readFile(config.rules)
      .split('\n')
      .map { _.trim }
      .filterNot(_.isEmpty)
    val results = lines.foldLeft(Seq[Result]()) {
      case (acc, l) =>
        val split = l.split('\t')
        val taskName = split.head
        val action = split.tail.head
        action match {
          case "hasOutput" =>
            val name = split.tail.tail.head
            split.tail.tail.tail.headOption match {
              case Some(size) =>
                split.tail.tail.tail.tail.headOption match {
                  case Some(range) =>
                    acc ++ rb.forall(
                      taskName,
                      tr =>
                        OutputFileSizeAbout(tr, name, size.toLong, range.toInt))
                  case None =>
                    acc ++ rb.forall(
                      taskName,
                      tr => OutputFileSize(tr, name, size.toLong))
                }
              case None =>
                acc ++ rb.forall(taskName, tr => OutputFileExists(tr, name))
            }
          case "atLeastOneHasOutput" =>
            val name = split.tail.tail.head
            split.tail.tail.tail.headOption match {
              case Some(size) =>
                split.tail.tail.tail.tail.headOption match {
                  case Some(range) =>
                    acc ++ rb.atLeastOne(
                      taskName,
                      tr =>
                        OutputFileSizeAbout(tr, name, size.toLong, range.toInt))
                  case None =>
                    acc ++ rb.atLeastOne(
                      taskName,
                      tr => OutputFileSize(tr, name, size.toLong))
                }
              case None =>
                acc ++ rb.atLeastOne(taskName, tr => OutputFileExists(tr, name))
            }
          case "stderrContains" =>
            val str = split.tail.tail.head
            acc ++ rb.forall(taskName, tr => StderrContains(tr, str))
          case "stdoutContains" =>
            val str = split.tail.tail.head
            acc ++ rb.forall(taskName, tr => StdoutContains(tr, str))
          case "atLeastOneStderrContains" =>
            val str = split.tail.tail.head
            acc ++ rb.atLeastOne(taskName, tr => StderrContains(tr, str))
          case "atLeastOneStdoutContains" =>
            val str = split.tail.tail.head
            acc ++ rb.atLeastOne(taskName, tr => StdoutContains(tr, str))
          case "aggregatedOutput" =>
            val name = split.tail.tail.head
            val size = split.tail.tail.tail.head
            split.tail.tail.tail.tail.headOption match {
              case Some(range) =>
                acc ++ rb.aggregate(
                  taskName,
                  tr =>
                    AggregatedOutputFileSizeAbout(tr,
                                                  name,
                                                  size.toLong,
                                                  range.toInt))
              case None =>
                acc ++ rb.aggregate(
                  taskName,
                  tr => AggregatedOutputFileSize(tr, name, size.toLong))
            }
          case _ =>
            throw new Exception(s"Unknown action '$action'")
        }
    }
    val (successes, failures) = results.partition(_.isSuccess)
    successes foreach { s =>
      println(s.message)
    }
    if (failures.nonEmpty)
      throw new Exception(
        s"The following rules did not succeed:\n${failures.map { _.message }.mkString("\n")}")
  }
}
