package no.uit.sfb.nextflow

sealed trait Result {
  def message: String
  def isSuccess: Boolean
}

case class Success(message: String) extends Result {
  val isSuccess = true
}

case class Failure(message: String) extends Result {
  val isSuccess = false
}
