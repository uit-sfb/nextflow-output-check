package no.uit.sfb.nextflow

import java.nio.file.Path

import no.uit.sfb.Config
import no.uit.sfb.scalautils.common.FileUtils

import scala.util.Try

case class TaskReport(
    name: String,
    hash: String,
    taskWorkdir: Path,
    failed: Boolean,
    exit: Int,
    stdout: String,
    stderr: String,
    outputs: Map[Path, Long] //File size in bytes
)

class TaskReportBuilder(config: Config) {
  def from(taskTrace: TaskTrace): TaskReport = {
    val taskWorkdir = {
      val split = taskTrace.hash.split('/')
      val firstLevelDir = config.workdir.resolve("work").resolve(split.head)
      val dirs =
        FileUtils.ls(firstLevelDir)._1
      dirs
        .find(_.getFileName.toString.startsWith(split.last))
        .getOrElse(throw new Exception(
          s"Could not find directory starting with '${split.last}' in $firstLevelDir"))
    }
    val failed = Seq("FAILED", "ABORTED").contains(taskTrace.status)
    val outputs: Map[Path, Long] = {
      if (FileUtils.exists(taskWorkdir.resolve("out"))) {
        Try {
          (FileUtils.filesUnder(taskWorkdir.resolve("out")) map { p =>
            (p, FileUtils.size(p))
          }).toMap
        } getOrElse {
          println(
            "Some directories are reported empty because an exception was raised, but they may contain files.")
          Map()
        } //For some reason, filesUnder fails if parent dir is a symlink
      } else
        Map()
    }
    TaskReport(
      taskTrace.name,
      taskTrace.hash,
      taskWorkdir,
      failed,
      Try { taskTrace.exit.toInt } getOrElse (-1),
      Try { FileUtils.readFile(taskWorkdir.resolve(".command.out")) }
        .getOrElse(""),
      Try { FileUtils.readFile(taskWorkdir.resolve(".command.err")) }
        .getOrElse(""),
      outputs
    )
  }
}
