package no.uit.sfb.nextflow

import no.uit.sfb.scalautils.common.FileUtils

trait RuleLike {
  def description: String
  protected def check: String //Returns empty string if successful
  def success: Boolean = check.isEmpty
  def checkRule(): Result = {
    if (success)
      Success(s"$description -- ok")
    else
      Failure(s"$description -- $check")
  }
}

case class StdoutContains(tr: TaskReport, str: String) extends RuleLike {
  val description =
    s"Task '${tr.name}' (${tr.hash}): stdout should contain '$str'"
  protected lazy val check = {
    if (!tr.stdout.contains(str))
      "not found"
    else ""
  }
}

case class StderrContains(tr: TaskReport, str: String) extends RuleLike {
  val description =
    s"Task '${tr.name}' (${tr.hash}): stderr should contain '$str'"
  protected lazy val check = {
    if (!tr.stderr.contains(str))
      "not found"
    else ""
  }
}

case class OutputFileExists(tr: TaskReport, relativePath: String)
    extends RuleLike {
  val description =
    s"Task '${tr.name}' (${tr.hash}): output file '$relativePath' should exist"
  protected lazy val check = {
    if (!tr.outputs.keySet.exists(_.toString.endsWith(relativePath)))
      "file does not exist"
    else ""
  }
}

trait OutputFileSizeLike extends RuleLike {
  def tr: TaskReport
  def relativePath: String
  protected lazy val check = {
    val matches = tr.outputs.filter {
      case (p, _) => p.toString.endsWith(relativePath)
    }
    if (matches.isEmpty)
      " -- file does not exist"
    else {
      val s = matches.foldLeft(0L) { case (acc, f) => acc + f._2 }
      isSizeOk(s)
    }
  }

  protected def isSizeOk(s: Long): String
}

trait SizeEquals {
  def size: Long
  protected def isSizeOk(s: Long): String = {
    if (s != size)
      s"$s doesn't match $size"
    else ""
  }
}

trait SizeInRange {
  def size: Long
  def range: Int
  protected def isSizeOk(s: Long): String = {
    val tol = (size * range) / 100
    if (s > size + tol || s < size - tol)
      s"$s not in range [${size - tol}}, ${size + tol}}]"
    else ""
  }
}

case class OutputFileSize(tr: TaskReport, relativePath: String, size: Long)
    extends OutputFileSizeLike
    with SizeEquals {
  val description =
    s"Task '${tr.name}' (${tr.hash}): output file '$relativePath' should exist and size should be $size bytes"
}

case class OutputFileSizeAbout(tr: TaskReport,
                               relativePath: String,
                               size: Long,
                               range: Int = 10)
    extends OutputFileSizeLike
    with SizeInRange {
  assert(range >= 0)
  val description =
    s"Task '${tr.name}' (${tr.hash}): output file '$relativePath' should exist and size should be $size (+/- $range%)"
}

case class OutputFileContains(tr: TaskReport, relativePath: String, str: String)
    extends RuleLike {
  override val description =
    s"Task '${tr.name}' (${tr.hash}): output file '$relativePath' should exist and should contain '$str'"
  protected lazy val check = {
    tr.outputs.keySet.find(_.toString.endsWith(relativePath)) match {
      case Some(p) =>
        if (FileUtils.readFile(p).contains(str))
          s"not found"
        else ""
      case None => "file does not exist"
    }
  }
}

trait AggregatedFileSizeLike extends RuleLike {
  def trs: Seq[TaskReport]
  def relativePath: String
  protected lazy val check = {
    val aggrSize = trs.foldLeft(0L) {
      case (acc, tr) =>
        val matches = tr.outputs.filter {
          case (p, _) => p.toString.endsWith(relativePath)
        }
        val s = matches.foldLeft(0L) { case (acc, f) => acc + f._2 }
        acc + s
    }
    isSizeOk(aggrSize)
  }

  protected def isSizeOk(s: Long): String
}

case class AggregatedOutputFileSize(trs: Seq[TaskReport],
                                    relativePath: String,
                                    size: Long)
    extends AggregatedFileSizeLike
    with SizeEquals {
  val description =
    s"Tasks '${trs.map { _.name }.mkString(", ")}': output file '$relativePath' aggregated size should be $size bytes"
}

case class AggregatedOutputFileSizeAbout(trs: Seq[TaskReport],
                                         relativePath: String,
                                         size: Long,
                                         range: Int = 10)
    extends AggregatedFileSizeLike
    with SizeInRange {
  val description =
    s"Tasks '${trs.map { _.name }.mkString(", ")}': output file '$relativePath' aggregated size should be $size (+/- $range%)"
}
