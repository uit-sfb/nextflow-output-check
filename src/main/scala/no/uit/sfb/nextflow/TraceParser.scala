package no.uit.sfb.nextflow

import java.nio.file.Path

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.csv.{CsvMapper, CsvSchema}
import no.uit.sfb.scalautils.json.Json

case class TaskTrace(
    task_id: String,
    hash: String,
    native_id: String,
    name: String,
    status: String,
    exit: String,
    submit: String,
    duration: String,
    realtime: String
)

object TraceParser {
  type Trace = Seq[TaskTrace]
  def fromTsv(path: Path): Trace = {
    val csvSchema =
      CsvSchema.builder().setUseHeader(true).setColumnSeparator('\t').build()
    val csvMapper = new CsvMapper()

    val readAll = csvMapper
      .readerFor(classOf[java.util.Map[String, String]])
      .`with`(csvSchema)
      .readValues(path.toFile)
      .readAll()

    val mapper = new ObjectMapper()

    // json return value
    Json.parse[Seq[TaskTrace]](
      mapper.writerWithDefaultPrettyPrinter().writeValueAsString(readAll))
  }
}
