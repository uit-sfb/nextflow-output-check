package no.uit.sfb

import java.nio.file.{Path, Paths}

case class Config(
    workdir: Path = Paths.get("."),
    traceName: String = "trace.txt",
    rules: Path = Paths.get(".")
)
