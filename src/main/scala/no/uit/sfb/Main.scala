package no.uit.sfb

import java.io.File

import scopt.OParser
import buildinfo.BuildInfo
import no.uit.sfb.nextflow.{DSL, RuleBuilder, WorkflowReportBuilder}

object Main extends App {
  try {
    val builder = OParser.builder[Config]

    val name = BuildInfo.name
    val ver = BuildInfo.version
    val gitCommitId = None

    val parser = {
      import builder._
      OParser.sequence(
        programName(name),
        head(name, ver),
        head(s"git: ${gitCommitId.getOrElse("unknown")}"),
        help('h', "help")
          .text("prints this usage text"),
        version('v', "version")
          .text("prints the version"),
        cmd("check-rules")
          .text("Check rules")
          .children(
            opt[File]("workdir")
              .required()
              .action((x, c) => c.copy(workdir = x.toPath))
              .text("Path to workdir. ~ cannot be used"),
            opt[File]("rules")
              .required()
              .action((x, c) => c.copy(rules = x.toPath))
              .text("Path to rules file. ~ cannot be used"),
            opt[String]("traceName")
              .action((x, c) => c.copy(traceName = x))
              .text("Trace file name. ~ cannot be used")
          )
      )
    }

    OParser.parse(parser, args, Config()) match {
      case Some(config) =>
        val dsl = new DSL(config)
        dsl.check()
      case _ =>
        // arguments are bad, error message will have been displayed
        throw new IllegalArgumentException()
    }
  } catch {
    case e: Throwable =>
      println(e.printStackTrace())
      sys.exit(1)
  }
}
