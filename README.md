# nextflow-output-check

Tool for validating output of NextFlow workflows against reference output.

## DSL

Note: The spaces are tab characters.
Parameters must NOT include tab characters.

```
<TaskName> stdoutContains|stderrContains <str>
<TaskName> atLeastOneStdoutContains|atLeastOneStderrContains <str>
<TaskName> hasOutput <str> [size] [range]
<TaskName> atLeastOneHasOutput <str> [size] [range]
<TaskName> aggregatedOutput <str> <size> [range]
```

Save the rules in a .tsv file passed to the program via `--rules` parameter.

## Usage

`docker run --rm -v <pathToWorkdir>:/workdir -v <pathToRules>:/rules.tsv registry.gitlab.com/uit-sfb/nextflow-output-check/nextflow-output-check:<version> -- check-rules [--traceName <nameOfTraceFile>=trace.txt]`